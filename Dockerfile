FROM bellsoft/liberica-openjdk-alpine:17.0.8

# Instal curl and jq
RUN apk add curl jq
# create workspace
WORKDIR /home/selenium-docker

# add the required files
ADD target/docker-resources     ./
ADD runner.sh                   runner.sh

# Start the runner.sh
ENTRYPOINT sh runner.sh

# Environment Variables
# BROWSER
# HUB_HOST
# TEST_SUITE
# THREAD_COUNT

# Run the tests
# ENTRYPOINT  java -cp 'libs/*' \
#             -Dselenium.grid.enabled=true \
#             -Dselenium.grid.hubHost=${HUB_HOST} \
#             -Dbrowser=${BROWSER} \
#             org.testng.TestNG \
#             test-suites/${TEST_SUITE} \
#             -threadcount ${THREAD_COUNT}